#!/bin/bash
# TEST.sh
#   by Lut99
#
# Created:
#   26 Jun 2024, 10:40:58
# Last edited:
#   27 Jun 2024, 14:21:42
# Auto updated?
#   Yes
#
# Description:
#   A meta-tester that tests if the `eflint-test.py`-script works on all
#   `tests/`.
#


##### INIT #####
# Make sure we're in the script location
cd "$(dirname "$0")"

# Parse args
debug=0
for arg in $@; do
    if [[ "$arg" == "--debug" ]]; then
        debug=1
    else
        2>&1 echo "Unknown option '$arg'"
    fi
done



##### RUNNING #####
# Run all things in test
for file in ./tests/*; do
    # Call the exec
    if [[ "$debug" -eq 1 ]]; then
        ./eflint-test.py "$file" --strict --debug || exit "$?"
    else
        ./eflint-test.py "$file" --strict || exit "$?"
    fi
done

# If we made it here, all good
echo ""
echo "All tests OK"
echo ""
exit 0
