# CHANGELOG for the UNIT-TESTER
This file keeps track of notable changes to the eFLINT Unit Tester script.

This project uses [semantic versioning](https://semver.org). This means that major versions number indicate **\[breaking changes\]**.


## v0.3.0 - TODO
### Added
- The script now supports testing all `.eflint` files in a directory.
    - Which files are matched is controlled by the `-x`/`--extension` option.

### Changed
- The script now tests files multi-threaded.
    - How many threads can be controlled with the `-j`/`--jobs` option.
    - As a consequence, all output is deferred until after execution.
- Tests are now always executed alphabetically.

### Fixed
- The reasoner output parser interpreting `+ AKA keychar('+')` as a create-effect and failing to parse it as an instance.

### Known bugs
- The output formatting sometimes fails if the line is very long and contains ANSI-characters.


## v0.2.0 - 2024-08-19
This release sees the addition of wildcards.

### Added
- The ability to parse `*` as a special type of instance, which matches _all_ instances.
    - This allows one to specify "any instance of the shape"-kind of assertions.

### Changed
- The script now tells of any counter-evidence of an instance that holds or violation that occurs if a negative assertion was encountered.
    - This allows one to see which of a matched wildcards violates the assertion.
- The script now warns about violations that occur which aren't asserted.
- Improved performance by parsing reasoner effects only once instead of on every check/violation assertion.

### Fixed
- Line numbers being 1 too low.
- Violated negative assertions (`//#assert !`) stating the error message of violated positive assertions.


## v0.1.0 - 2024-06-27
Initial release!

### Added
- The `eflint-test.py` script that preprocess eFLINT files to check for some assertions as it goes.
- Support for `//#assert [!]<instance>.`, which checks if a particular instance holds after any previous phrase has been processed (or _not_ holds, if `!` is given).
    - NOTE: For now, obfuscation is equated to termination.
- Support for `//#violated [!]<instance>.`, which checks if a particular instance causes a violation after any previous phrase has been processed (or does _not_ cause violations, if `!` is given).
